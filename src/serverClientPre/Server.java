package serverClientPre;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Server {
	//@SuppressWarnings({ "resource" })
	@SuppressWarnings("resource")
	public static void main(String args[]) {
		System.out.println("## SERVER \n----------------\n");
		
		try {
			ServerSocket server = new ServerSocket(1111);
			
			Socket clientSocket = server.accept();
			BufferedReader inputStream  = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			PrintStream outputStream = new PrintStream(clientSocket.getOutputStream());
			
			Scanner scanner = new Scanner(System.in);
			String clientMessage;
			
			while (true) {
				clientMessage = inputStream.readLine();
				System.out.println("Client: "+clientMessage);
				
				System.out.print("Server: ");
				String string = scanner.nextLine();
				outputStream.println(string);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}