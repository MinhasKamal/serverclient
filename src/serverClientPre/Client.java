/**
 * Developer: Minhas Kamal (BSSE0509)
 */

package serverClientPre;

import java.io.*;
import java.net.*;
import java.util.Scanner;
public class Client {
    @SuppressWarnings("resource")
	public static void main(String[] args) {
    	System.out.println("## CLIENT \n----------------\n");
    	
    	try {
			Socket smtpSocket = new Socket("10.255.6.101", 1111);
			BufferedReader inputStream  = new BufferedReader(new InputStreamReader(smtpSocket.getInputStream()));
			DataOutputStream outputStream = new DataOutputStream(smtpSocket.getOutputStream());

			Scanner scanner = new Scanner(System.in);
			String serverMessage;
			
			//outputStream.writeBytes("Hellow!! Server!!\n");
			while (true){
				System.out.print("Client: ");
				String string = scanner.nextLine();
				outputStream.writeBytes(string+"\n");
				
				serverMessage = inputStream.readLine();
				System.out.println("Server: "+serverMessage);
			}
			
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}