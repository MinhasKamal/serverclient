package serverClientPro;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Server {
	private ServerSocket server;
	private Socket clientSocket;
	private BufferedReader inputStream;
	private PrintStream outputStream;

	public Server() throws Exception {
		server = new ServerSocket(1111);
		clientSocket = server.accept();
		inputStream  = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		outputStream = new PrintStream(clientSocket.getOutputStream());

	}
	
	Thread listen = new Thread(new Runnable() {
		String clientMessage;
	     public void run() {
	    	try{
		    	 while (true){
		    		 clientMessage = inputStream.readLine();
					 System.out.println("Client: "+clientMessage);
					 
					 Thread.sleep(300);
		    	 }
	    	}catch (Exception e) {
	            e.printStackTrace();
	        }
	     }
	});  
	
	Thread speak = new Thread(new Runnable() {
		Scanner scanner = new Scanner(System.in);
	     public void run() {
	    	try{
		    	 while (true){
					String string = scanner.nextLine();
					System.out.println("Server: " + string);
					outputStream.println(string);
		    	 }
	    	}catch (Exception e) {
	            e.printStackTrace();
	        }
	     }
	}); 
	
	public static void main(String args[]) {
		System.out.println("## SERVER \n----------------\n");
		
		try {
			Server server = new Server();
    		
			server.listen.start();
			server.speak.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}