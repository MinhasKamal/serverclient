/**
 * Developer: Minhas Kamal (BSSE0509)
 */

package serverClientPro;

import java.io.*;
import java.net.*;
import java.util.Scanner;
public class Client {
	private Socket smtpSocket;
	private BufferedReader inputStream;
	private DataOutputStream outputStream;
	
	public Client() throws Exception {
		smtpSocket = new Socket("10.255.4.181", 1111);
		inputStream  = new BufferedReader(new InputStreamReader(smtpSocket.getInputStream()));
		outputStream = new DataOutputStream(smtpSocket.getOutputStream());
		
	}
	
	Thread listen = new Thread(new Runnable() {
		String serverMessage;
	     public void run() {
	    	try{
		    	 while (true){
			    	 serverMessage = inputStream.readLine();
					 System.out.println("Server: "+serverMessage);
					 
					 Thread.sleep(300);
		    	 }
	    	}catch (Exception e) {
	            e.printStackTrace();
	        }
	     }
	});  
	
	Thread speak = new Thread(new Runnable() {
		Scanner scanner = new Scanner(System.in);
	     public void run() {
	    	try{
		    	 while (true){
					 String string = scanner.nextLine();
					 System.out.println("Client: " + string);
					 outputStream.writeBytes(string+"\n");
		    	 }
	    	}catch (Exception e) {
	            e.printStackTrace();
	        }
	     }
	}); 
	
	public static void main(String[] args) {
    	System.out.println("## CLIENT \n----------------\n");
    	
    	try {
    		Client client = new Client();
    		
    		client.listen.start();
    		client.speak.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}