/**
* Developer: Minhas Kamal(BSSE-0509, IIT, DU)
**/

package serverClient;

import javax.swing.*;


/**
 * 
 * 
 * @author Minhas Kamal
 */
@SuppressWarnings("serial")
public class ClientGui extends JFrame {
	//**
	// Variable Declaration 																	#*******D*******#
	//**
	JTextField jTextField;
	JButton jButton;
	JTextArea jTextArea;
	// End of Variable Declaration 																#_______D_______#

	/***##Constructor##***/
	public ClientGui() {

		initialComponent();
	}

	
	/**
	 * Method for Initializing all the GUI variables and placing them all to specific space on 
	 * the frame. It also specifies criteria of the main frame.
	 */
	private void initialComponent() {
		//**
		// Initialization 																		#*******I*******#
		//**
		jTextField = new JTextField();
		jButton = new JButton("Send");
		jTextArea = new JTextArea();
		// End of Initialization																#_______I_______#

		//**
		// Setting Bounds and Attributes of the Elements 										#*******S*******#
		//**
		jTextField.setBounds(0, 0, 240, 30);
		jButton.setBounds(240, 0, 60, 30);
		jTextArea.setBounds(0, 40, 300, 300);
		jTextArea.setEditable(false);
		// End of Setting Bounds and Attributes 												#_______S_______#
		
		//**Setting Criterion of the Frame**//
		setIconImage(new ImageIcon(getClass().getResource("")).getImage());
		setBounds(200, 200, 305, 370);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(null);
		setResizable(false);
		
		//**
		// Adding Components 																	#*******A*******#
		//**
		add(jTextField);
		add(jButton);
		add(jTextArea);
		// End of Adding Components 															#_______A_______#
	}

	/********* Main Method *********/
	public static void main(String args[]) {
		/*// Set the NIMBUS look and feel //*/
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception ex) {
			// do nothing if operation is unsuccessful
		}

		/* Create and display the form */
		ClientGui gui = new ClientGui();
		gui.setVisible(true);
	}
}
