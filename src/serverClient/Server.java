/**
 * Developer: Minhas Kamal (BSSE0509)
 */

package serverClient;

import java.io.*;
import java.net.*;

public class Server {
	@SuppressWarnings({ "resource" })
	public static void main(String args[]) {
		System.out.println("## SERVER \n----------------\n");
		
		try {
			ServerSocket server = new ServerSocket(1111);
			
			Socket clientSocket = server.accept();
			BufferedReader inputStream  = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			PrintStream outputStream = new PrintStream(clientSocket.getOutputStream());
			
			String clientMessage;
			while (true) {
				clientMessage = inputStream.readLine();
				
				System.out.println("Client: " + clientMessage);
				
				outputStream.println("Hi! How are you?");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}