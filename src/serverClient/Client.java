/**
* Developer: Minhas Kamal(BSSE-0509, IIT, DU)
**/

package serverClient;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.*;


public class Client{
	// GUI Declaration
	private ClientGui gui;
	
	//**
	// Variable Declaration 																	#*******D*******#
	//**
	JTextField jTextField;
	JButton jButton;
	JTextArea jTextArea;
	
	////
	Socket smtpSocket;
	BufferedReader inputStream;
	DataOutputStream outputStream;
	// End of Variable Declaration 																#_______D_______#

	/***##Constructor##***/
	public Client() {
		try{
			smtpSocket = new Socket("10.255.5.106", 1111);
			inputStream  = new BufferedReader(new InputStreamReader(smtpSocket.getInputStream()));
			outputStream = new DataOutputStream(smtpSocket.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		initialComponent();
		
		activate();
	}

	
	/**
	 * Method for Initializing all the GUI variables and placing them all to specific space on 
	 * the frame. It also specifies criteria of the main frame.
	 */
	private void initialComponent() {
		// GUI Initialization
		gui = new ClientGui();
		gui.setTitle("Client");
		gui.setVisible(true);
		
		//**
		// Assignation 																			#*******A*******#
		//**
		jTextField = gui.jTextField;
    	jButton = gui.jButton;
    	jTextArea = gui.jTextArea;
		// End of Assignation																	#_______A_______#

		//**
		// Adding Action Events & Other Attributes												#*******AA*******#
		//**
    	jButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButtonActionPerformed(evt);
			}
		});
		// End of Adding Action Events & Other Attributes										#_______AA_______#
	}

	//**
	// Action Events 																			#*******AE*******#
	//**
	private void jButtonActionPerformed(ActionEvent evt) {
		try {
			String string = jTextField.getText();
			
			outputStream.writeBytes(string);
			jTextArea.append("Client: " + string + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// End of Action Events 																	#_______AE_______#

	//**
	// Auxiliary Methods 																		#*******AM*******#
	//**
	private void activate() {
		try {
			outputStream.writeBytes("yo man");
			jTextArea.append("Client: yo man \n");
			
			String serverMessage;
			while (true) {
				serverMessage = inputStream.readLine();
				jTextArea.append("Server: " + serverMessage + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void close() throws Exception {
		outputStream.close();
		inputStream.close();
		smtpSocket.close();
	}
	// End of Auxiliary Methods 																#_______AM_______#
	
	//**
	// Overridden Methods 																		#*******OM*******#
	//**
	
	// End of Overridden Methods 																#_______OM_______#
	
	
	/********* Main Method *********/
	public static void main(String args[]) {
		/*// Set the NIMBUS look and feel //*/
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception ex) {
			// do nothing if operation is unsuccessful
		}

		/* Create */
		new Client();
	}
}
