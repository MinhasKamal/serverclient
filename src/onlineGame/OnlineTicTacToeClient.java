package onlineGame;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

public class OnlineTicTacToeClient {
	static char[][] move = new char[3][3];
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
        int i, j, k=49;
        for(i=0; i<3; i++){  //assigning values in the box
            for(j=0; j<3; j++)
            {
                move[i][j]=(char) k;
                k++;
            }
        }
        
        try {
        	client_choice();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
        scanner.close();
        System.out.print("\n\n\t*** GAME ENDS ***\n");
	}
	
	
	static void client_choice() throws Exception  //this function takes the input of the players & arranges other function works
	{
		Scanner scanner = new Scanner(System.in);
		
		Socket smtpSocket = new Socket("10.255.6.101", 1111);
		BufferedReader inputStream  = new BufferedReader(new InputStreamReader(smtpSocket.getInputStream()));
		DataOutputStream outputStream = new DataOutputStream(smtpSocket.getOutputStream());
		
	    int i=0;
	    while(i<9)  //the loop will give at most 35 chances
	    {
	        introduction();
	        System.out.print("\n##**SERVER vs. CLIENT\n\n");
	        board();  //2nd function
	
	        int pl=i%2+1;
	        String name;
	        if(pl==1) name="client";
	        else name="server";
	
	        System.out.print("## Player-"+pl+": "+name+"\n");
	
	        
        	while(true){  //the loop will assure valid choice
        
	            int sel;  //player's selection
	
	            if(pl==1){
	            	System.out.println("Select a cell: ");
	            	sel=scanner.nextInt();
					outputStream.writeBytes(sel+"\n");
		        }
		        else{
		        	System.out.println("Waiting for selection: ");
		        	String str = inputStream.readLine();
		        	System.out.println(str);
		        	sel = Integer.parseInt(str);
		        }
	
	            System.out.print("\n");
	
	            int matched=0;
	
	            int k, j;
	            for(k=0; k<3; k++)
	            {
	                for(j=0; j<3; j++)
	                    if(move[k][j]-48==sel)
	                    {
	                        if(pl==1)move[k][j]='X';
	                        else move[k][j]='O';
	                        matched=1;
	                        break;
	                    }
	                if(matched>0) break;
	            }
	
	            if(matched==0)
	            {
	            	System.out.print("\nInvalid move!\n");
	                continue;
	            }
	
	            break;
        	}
	
	        if(varification()>0)
	        {
	            introduction();
	            board();
	            System.out.print("\n"+(char)1+"\t**## Player-"+pl+": "+name+" wins! ##**\n");
	            break ;
	        }
	        else if(i==8)
	        {
	            introduction();
	            board();
	            System.out.println("\n**The match is draw.\n");
	            break ;
	        }
	
	        i++;
	    }
	    
	    scanner.close();
	    smtpSocket.close();
	}
	
	/****/
	
	static void board()
	{
	    int i, j;
	    System.out.print("\n\n");
	    for(i=0; i<3; i++)
	    {
	        if(i>0)
	        {
	        	System.out.print("\n  ---+---+---\n");
	        }
	        System.out.print("  ");
	        for(j=0;j<3;j++)
	        {
	        	System.out.print(" " + (char)move[i][j] + " ");
	            if(j!=2) System.out.print("|");
	        }
	    }
	    System.out.print("\n\n");
	}
	
	static void introduction(){  //this function prints the introduction of the game
	    System.out.print("\n\n\n\n\t\t*** CRISS-CROSS GAME ***\n\n\n");  //still image
	
	    return ;
	}
	
	/****/
	
	static int varification(){
	    int i;
	
	    for(i=0; i<3; i++)  //row & column match
	    {
	        if(move[i][0]==move[i][1] && move[i][0]==move[i][2]) return 1;
	        if(move[0][i]==move[1][i] && move[0][i]==move[2][i]) return 1;
	    }
	
	    if(move[0][0]==move[1][1] && move[1][1]==move[2][2]) return 1;
	
	    if(move[0][2]==move[1][1] && move[1][1]==move[2][0]) return 1;
	
	    return 0;
	}
}
